<?php
/*
 *	This file receives post data from the client and uses it to create a number of posts. 
 *	It utilizes native wordpress as well as ACF methods.
 *	After injecting the correct amount it sends a response back to the client.
 */


//
// set vars
//

$amount = intval( $_POST['amount'] );
$post_name = str_replace(' ', '_', $_POST['base-name']);
$post_title = $_POST['base-name'];
$start = intval($_POST['start']);
$chunk_end = intval($_POST['chunkEnd']);
$post_type = sanitize_title( $_POST['post-type'] );
$make = $amount >= $start + $chunk_end - 1 ? $start + $chunk_end -1 : $amount;
// array to store testing data so we can log it on the front end.
// $testing_data = array();

//
// end set vars
//





//
// helper functions -- move this to another file if more than 2 get added.
//

// we get all the metadata associated with this post type
function get_posts_acf_fields($post_id, $post_type){
	$fields = array();

	// get the metabox ids
	$filter = array( 
		'post_id'	=> $post_id, 
		'post_type'	=> $post_type
	);
	$metabox_ids = array();
	$metabox_ids = apply_filters( 'acf/location/match_field_groups', $metabox_ids, $filter );
	//
	//
	
	// get the fields for each metabox id and make them one array.
	if (!empty($metabox_ids)){ 
		foreach ($metabox_ids as $m_id){
			$meta_box_fields = apply_filters('acf/field_group/get_fields', array(), $m_id);
			$fields = array_merge($fields, $meta_box_fields);
		}
	}
	//
	//
	
	return $fields;
}

/**
 * Checks the field type and returns the appropriate value
 * @param  array $field
 * @return mixed        an appropriate value based on the field type.
 */
function get_field_values($field){

	switch ($field['type']){
		case 'wysiwyg':
			$val = '<p>Compellingly promote innovative systems after future-proof schemas. Efficiently utilize end-to-end expertise for synergistic action items. Appropriately target virtual total linkage rather than interactive methods of empowerment. </p>';
			break;
		case 'true_false':
			$val = 1;
			break;
		case 'image':
			// use this for now, but do a modded singleton later
			// like have an empty image, if its empty run this and set it,
			// if not just use what weve got.
			$ids = get_posts( 
				array(
					'post_type'      => 'attachment', 
					'post_mime_type' => 'image', 
					'post_status'    => 'inherit', 
					'posts_per_page' => 1,
					'fields'         => 'ids',
			));
			$val = $ids[0];
			break;
		case 'relationship':
			$val = array();
			if (!empty($field['post_type'][0])){
				$rel_post_type = $field['post_type'][0];
			} else {
				$rel_post_type = 'post';
			}
			$rel_posts = get_posts( array( 'posts_per_page' => 2, 'post_type' => $rel_post_type ) );
			foreach($rel_posts as $rel_post){
				$val[] = $rel_post->ID;
			}
			break;
		case 'text':
			$val = 'Progressively generate customized value with holistic models';
			break;
		case 'textarea':
			$val = 'Assertively create client-centered total linkage with cutting-edge quality vectors. Continually optimize exceptional collaboration and idea-sharing via progressive deliverables.';
			break;
		case 'number':
			$val = 2;
			break;
		case 'password':
			// could be more secure but this has never been used by a project here.
			// TODO: make random.
			$val = 'p455w0rd';
			break;
		case 'email':
			$val = 'test@example.com';
			break;
		case 'repeater':
			$val = get_field($field['key'], $new_post_id);
			$sub_vals = array();
			foreach ( $field['sub_fields'] as $sub ){
				$sub_vals[$sub['name']] = get_field_values($sub);
			}
			$val[] = $sub_vals;
			break;
		case 'file':
			$ids = get_posts( 
				array(
					'post_type'      => 'attachment', 
					'post_mime_type' => 'image', 
					'post_status'    => 'inherit', 
					'posts_per_page' => 1,
					'fields'         => 'ids',
			));
			$val = $ids[0];
			break;
		case 'select':
		case 'radio':
		case 'checkbox':
			$val = array_rand($field['choices'], 1);
			break;
		default:
			$val = '';
			break;

	}
	return $val;
}
//
// End helper functions
//


//
// Create the posts
//
for ($i=$start; $i <= $make; $i++) { 

	//$testing_data['postNo'][$i]['count'] = $i;

	$new_post_id = wp_insert_post(
				array(
					'comment_status'	=>	'closed',
					'ping_status'		=>	'closed',
					'post_content'		=>	'<p>Objectively incubate virtual applications vis-a-vis e-business systems. Progressively customize global models before team driven ideas. Authoritatively innovate team driven metrics with distributed value. Appropriately deploy enterprise-wide deliverables vis-a-vis market positioning e-tailers.</p>', 
					'post_author'		=>	get_current_user_id(),
					'post_name'			=>	$post_name . '_' . $i,
					'post_title'		=>	$post_title . ' ' . $i,
					'post_status'		=>	'publish',
					'post_type'			=>	$post_type
				)
		);

	//$testing_data['postNo'][$i]['newID'] = $new_post_id;

	$fields = get_posts_acf_fields($new_post_id, $post_type);

	// loop through the fields
	// we will enter data depending on the type
	foreach ($fields as $field){



		// supports the following fields.
		// 
		// 
		// "basic"
			// text
			// textarea
			// email
			// password
			
		// "content"
			// 	wysiwyg	
			// 	file
			// 	image

		// "choice"
			// select
			// checkbox
			// radio button
			// true_false
		
		// "relational"
			// relationship
		// 


		$val = get_field_values( $field );
		

		
		update_field($field['key'], $val, $new_post_id);
	}

}

//
// End create the posts
//




//
// Send JSON back to the client
//
echo json_encode( array('success' => true, 'made' => $i - 1) );
//
// End send JSON
//