<?php
/**
 * Plugin Name: Advanced Test Data
 * Description: Automatically generate test data for custom post types and custom fields created by ACF
 * Version: 1.0.0
 * Author: VIUS
 * License: GPL2
 * Text Domain: at-data
 */

// add to the menu
	add_action('admin_menu', 'add_at_data_menu');
	function add_at_data_menu(){
		add_menu_page( 'Advanced Test Data Generator', 'Advanced Test Data Generator', 'manage_options', 'at-data' , 'display_at_data' );
	}
//


// display the form
function display_at_data(){
	require_once(plugin_dir_path( __FILE__ ) . 'base.php');
}
//

// ajax response - process the request - create the test posts
add_action( 'wp_ajax_at_data', 'at_data_processing' );
function at_data_processing() {
	require_once(plugin_dir_path( __FILE__ ) . 'at-data-processing.php');
	wp_die();
}
//

// load stylesheet
wp_enqueue_style( 'at-css', plugins_url( 'at-style.css' , __FILE__ ) );
//

?>