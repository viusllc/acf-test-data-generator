# ACF Test Data Generator #

## To use ##
Upload files to the Wordpress plugins directory. Requires ACF. If you plan on using post types that support image or file fields, please make sure you have at least one image loaded into the Wordpress media library.

## Description ##
Creates generic test data by allowing a user to specify a post type, the number of posts and the name of the post, the name will be repeated with its count appended to it. It enters the name, the content, and ACFs associated with the post type.

  
### Supported ACF Data ###

* text
* textarea
* email
* password
* wysiwyg
* image ( must have at least 1 file in the media library )
* file  (must have at least 1 file in the media library )
* select
* checkbox
* radio button
* true_false
* relationship ( you cannot form relationships to posts that do not exist )
* Repeater ( repeater using the above fields )