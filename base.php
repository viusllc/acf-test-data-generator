<?php wp_head(); ?>
<section id="at-content">
	<header>
		<h1><?php _e('Advanced Test Data Generator', 'at-data'); ?></h1>
	</header>

	<div class="message"></div>
	
	<progress max="160" value="1"></progress>
	
	<div class="instructions">
		<h2><?php _e('This supports the following ACF fields: ','at-data'); ?></h2>
		<ul>
			<li class="head"><?php _e('Basic','at-data'); ?></li>
			<li><?php _e('text','at-data'); ?></li>
			<li><?php _e('textarea','at-data'); ?></li>
			<li><?php _e('email','at-data'); ?></li>
			<li><?php _e('password','at-data'); ?></li>
			<li class="head"><?php _e('Content','at-data'); ?></li>
			<li><?php _e('wysiwyg','at-data'); ?></li>
			<li class="note"><?php _e('for file and image please make sure you have images uploaded into the wordpress media library','at-data'); ?></li>
			<li><?php _e('file','at-data'); ?></li>
			<li><?php _e('image','at-data'); ?></li>
			<li class="head"><?php _e('choice','at-data'); ?></li>
			<li><?php _e('select','at-data'); ?></li>
			<li><?php _e('checkbox','at-data'); ?></li>
			<li><?php _e('radio button','at-data'); ?></li>
			<li><?php _e('true / false','at-data'); ?></li>
			<li class="head"><?php _e('Relational','at-data'); ?></li>
			<li><?php _e('relationship','at-data'); ?></li>
		</ul>
	</div>

	<form name="at-data-form" id="at-data-form" method="post">
		<?php $types = get_post_types(); ?>
		
		<div class="input-group">
			<label for="post-type-select"><?php _e('Post Type:','at-data'); ?></label>
			<select name="post-type" id="post-type-select" required>
				<?php foreach($types as $type): ?>
					<option value="<?php echo $type; ?>"><?php echo $type; ?></option>
				<?php endforeach; ?>
			</select>
		</div>


		<div class="input-group">
			<label for="amount"><?php _e('Amount:','at-data'); ?></label>
			<input type="number" name="amount" id="amount" min="1" max="50" required>
		</div>

		<div class="input-group">
			<label for="base-name"><?php _e('Base Name:','at-data'); ?></label>
			<input type="text" id="base-name" name="base-name" required>	
		</div>

		<div class="input-group">
			<input type="submit" name="submit" id="submit">
		</div>
	</form>

	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
	<script type="text/javascript">

		( function($){
			
			// object to handle the recursive ajax
			$.AtdRequests = {
				
				// sets up the initial state and handle processing before making ajax call
				preProcess : function(){

					// make sure users can't click the submit twice
					$('#submit').prop('disabled', true);
					// 

					// set vars
					var rangeStart = 1;
					
					var amount = $('#amount').val();

					var maxChunk = 5;

					var chunks = amount / maxChunk;

					var data = {
								
						'post-type' : $('#post-type-select').val(),
						'base-name' : $('#base-name').val(),
						'action': 'at_data',
						'amount' : amount,
						'start' : 1,
						'chunkEnd' : maxChunk
					};
					//
					

					// change states
					$('progress').attr({'max' : amount});
					$('progress').attr({'value' : data['start']});
					$('.message').text('Beginning post creation, please stand by...');
					//
					
					// make the ajax call
					$.AtdRequests.loadPosts( data );
					//
				},

				// make the request to create more posts on the server
				// does recursive chunking.
				loadPosts : function( data ){
				
					$.post(ajaxurl, data, function(response) {
						response = $.parseJSON(response);
						console.log(response);
						$('progress').attr({"value" : response.made});
						if ( response.made < data.amount ){
							data['start'] = response.made + 1;
							$('.message').text(response.made + ' posts have been created, still loading...');
							$.AtdRequests.loadPosts( data );
						} else {
							$('.message').text('All ' + response.made + ' posts have been created. We are done here.' );
							$('#submit').prop('disabled', false);
						}
						
					});
				}
			};
			// end AtdRequests

			$('document').ready(function(){
				
				// validator code
				$.validator.addMethod("lettersonly", function(value, element) {
				  return this.optional(element) || /^[a-z\s]+$/i.test(value);
				}, "Letters and spaces only please"); 


				$('#at-data-form').validate({
					rules : {
						"base-name" : { lettersonly : true }
					},
					submitHandler: function( form ){

						$.AtdRequests.preProcess();
					}
				});
				// end validator code
				

			});
		})(jQuery);

	

	</script>
</section>